<?php

class TesController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'cek', 'login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('index', 'kumpul', 'pra', 'error'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'index', 'error'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCek()
    {
        if(isset($_GET['username']) && ($_GET['username'] != null)){
            $model = Contestant::model()->findByAttributes(array('contestant_username' => $_GET['username']));
            if($model == null) echo 'not found'; else echo 'ok';
        }
    }

    public function actionLogin()
    {
        $model = new Contestant;

        if(isset($_POST['Contestant']))
        {
            $model->attributes          = $_POST['Contestant'];
            $password                   = $model->contestant_password;
            $user                       = new UserIdentity('admin', '');
            $model->contestant_password = $user->encrypt($password);
            $model->problem_set_id      = Yii::app()->params['test_problem_set'];
            if(Contestant::model()->findByAttributes(array('contestant_username' => $model->contestant_username)) == null) $model->save();

            $user = new LoginForm;
            $user->username = $model->contestant_username;
            $user->password = $password;
            if($user->validate() && $user->login()){
                if (Yii::app()->user->id == 0) $this->redirect(array("site/admin"));
                else if((time() >= Yii::app()->params['contest_start'])) $this->redirect(array("index"));
                else $this->redirect(array("site/logout"));
            }
        }

        $this->render('login',array('model'=>$model));
    }

    public function actionPra()
    {
        $userModel = Contestant::model()->findByPk(Yii::app()->user->id);
        $userModel->contestant_start_time = $userModel->contestant_last_active = $userModel->contestant_last_submit = time();
        $userModel->save();
        $this->redirect(array('tes/index?new=true'));
    }

    public function actionIndex()
    {
        if(Yii::app()->user->isGuest) $this->redirect(array('tes/login'));

        $userModel = Contestant::model()->findByPk(Yii::app()->user->id);

        if($userModel->contestant_start_time == null){
            if(time() < Yii::app()->params['contest_end']) $this->render('preliminary');
            else $this->render('end');
        }
        else{
            $userModel->contestant_last_active = time();
            $userModel->save();

            //new login
            if($userModel->contestant_problem_worked == null){
                $this->generateProblem(0, $userModel);
                $userModel = Contestant::model()->findByPk(Yii::app()->user->id);
            }

            $worked = json_decode($userModel->contestant_problem_worked);

            //past time limit
            $i=1;
            while($userModel->contestant_last_submit + ($i * Yii::app()->params['section_duration']) <= time()) ++$i;
            if($i > 1){
                $userModel->contestant_last_submit += ($i - 1) * Yii::app()->params['section_duration'];
                $userModel->save();
                $this->generateProblem($worked->section + $i - 1, $userModel);
                $userModel = Contestant::model()->findByPk(Yii::app()->user->id);
                $worked = json_decode($userModel->contestant_problem_worked);
            }

            $contestEnd = Yii::app()->params['section_duration'] * (Yii::app()->params['total_section'] - $worked->section) + $userModel->contestant_last_submit;
            if(($worked->section >= Yii::app()->params['total_section']) || (sizeof($worked->problem) == 0)) $this->render('end');
            else{
                $problem = array();
                foreach($worked->problem as $id){
                    $model = Problem::model()->findByPk($id);
                    array_push($problem, $model);
                }

                $this->render('index', array(
                    'start'       => $worked->section * Yii::app()->params['problem_per_section'] + 1,
                    'problem'     => $problem,
                    'timeLeft'    => $contestEnd - time(),
                    'submitTimer' => $userModel->contestant_last_submit + Yii::app()->params['section_duration'] - time(),
                ));
            }
        }
    }

    public function actionKumpul()
    {
        if(isset($_POST['Answer'])){
            foreach($_POST['Answer'] as $answer){
                $model = new Answer;
                $model->attributes = $answer;

                $criteria = array(
                    'condition' => 'problem_id = '.$model->problem_id.' AND contestant_id = '.$model->contestant_id,
                );
                $modelOld = Answer::model()->find($criteria);

                if($modelOld == null) $model->save();
                else{
                    $modelOld->attributes = $answer;
                    $modelOld->update();
                }
            }
            if(isset($_GET['new'])){
                $user = Contestant::model()->findByPk(Yii::app()->user->id);
                $user->contestant_last_submit = time();
                $user->save();
                $used = json_decode($user->contestant_problem_worked);
                $this->generateProblem($used->section + 1, $user);
            }
            $this->redirect('index');
        }
    }

    public function actionLogout()
    {
        if(!Yii::app()->user->isGuest){
            $userModel = Contestant::model()->findByPk(Yii::app()->user->id);
            $userModel->contestant_is_login = 0;
            $userModel->save();
            Yii::app()->user->logout();
        }
        $this->redirect('login');
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    private function generateProblem($section, $user)
    {
        if($user->contestant_problem_used == null) $used = array();
        else $used = json_decode($user->contestant_problem_used);

        $source = array();
        $criteria = array(
            'condition' => 'problem_set_id = '.$user->problem_set_id,
        );
        $problem = Problem::model()->findAll($criteria);
        foreach($problem as $prob)
            if(!in_array($prob->problem_id, $used)) array_push($source, $prob->problem_id);

        $worked = array();
        while((sizeof($worked) < Yii::app()->params['problem_per_section']) && (sizeof($source) > 0)){
            $end = sizeof($source)-1;
            $idx = mt_rand(0, $end);
            $temp         = $source[$idx];
            $source[$idx] = $source[$end];
            array_pop($source);
            array_push($worked, $temp);
            array_push($used, $temp);
        }

        $result = array(
            'section' => $section,
            'problem' => $worked,
        );

        $user->contestant_problem_worked = json_encode($result);
        $user->contestant_problem_used = json_encode($used);
        $user->save();
    }
}