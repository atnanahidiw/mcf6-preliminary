<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login', 'logout', 'index', 'error'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('index', 'error'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'index', 'error'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    /**
     * Displays the login page
     */
    public function actionIndex()
    {
        if(!Yii::app()->user->isGuest){
            switch(Yii::app()->user->id)
            {
                case 0  : $this->redirect(array("site/admin"));  break;
                default : $this->redirect(array("/penyisihan"));
            }
        }
        else{
            $model=new LoginForm;

            // if it is ajax validation request
            if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if(isset($_POST['LoginForm']))
            {
                $model->attributes=$_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if($model->validate() && $model->login()){
                    //$redirectTest = Contestant::model()->findByPk(Yii::app()->user->id)->problem_set_id == Yii::app()->params['test_problem_set'];
                    //$redirectTest = $redirectTest && (Yii::app()->user->id != 0);
                    //if($redirectTest) $this->redirect(array("/tes"));
                    if (Yii::app()->user->id == 0) $this->redirect(array("site/admin"));
                    else if((time() >= Yii::app()->params['contest_start'])) $this->redirect(array("/penyisihan"));
                    else $this->redirect(array("site/logout"));
                }
            }
            // display the login form
            $this->render('login',array('model'=>$model));
        }
    }

    /**
     * Redirect to Index
     */
    public function actionLogin()
    {
        $this->redirect(array('index'));
    }

    /**
     * Show admin homepage.
     */
    public function actionAdmin()
    {
        $this->layout = 'admin';

        $this->render('admin');
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        if(!Yii::app()->user->isGuest){
            $userModel = Contestant::model()->findByPk(Yii::app()->user->id);
            $userModel->contestant_is_login = 0;
            $userModel->save();
            Yii::app()->user->logout();
        }
        $this->redirect(Yii::app()->homeUrl);
    }
}