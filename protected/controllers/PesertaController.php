<?php

class PesertaController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('aktif', 'error'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'index', 'nilai', 'sync', 'ubah', 'error'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->layout = 'admin';
        $criteria = array(
            'condition' => 'contestant_id > 0',
        );
        $model = Contestant::model()->findAll($criteria);
        $this->render('index',array(
            'model'=>$model
        ));
    }

    public function actionNilai()
    {
        $problem = Problem::model()->findAll();
        $key = array();
        foreach($problem as $prob) $key[$prob->problem_id] = $prob->problem_choice_right;

        $answer = Answer::model()->findall();
        foreach($answer as $ans){
            if($ans->answer_choice != ''){
                if($ans->answer_choice == $key[$ans->problem_id]) $ans->answer_score = 4; else $ans->answer_score = -1;
                $ans->update();
            }
        }

        $this->redirect(array('index'));
    }

    public function actionSync()
    {
        $source = 'http://www.math.itb.ac.id/mcf-mmc/home/sync';

        $jsondata = file_get_contents($source);
        $object = get_object_vars(json_decode($jsondata));
        foreach($object as $obj){
            $obj = get_object_vars($obj);
            $model = Contestant::model()->findByPk($obj['contestant_id']);
            if($model == null){
                $model = new Contestant;
                $model->attributes=$obj;
                $model->contestant_id=$obj['contestant_id'];
                $model->save();
            }
            else if($model->contestant_password != $obj['contestant_password']){
                $model->contestant_password = $obj['contestant_password'];
                $model->update();
            }
        }
        $this->redirect(array('index'));
    }

    public function actionUbah()
    {
        if(isset($_GET['id']) && ($_GET['id'] != null)){
            $model = Contestant::model()->findByPk($_GET['id']);
            if(isset($_POST['Contestant'])){
                $model->attributes=$_POST['Contestant'];
                $model->save();
                $this->redirect(array('index'));
            }
            $this->renderPartial('contestant_form', array(
                'id' => $_GET['id'],
                'model' => $model,
                'problemSet' => ProblemSet::model()->findAll(),
            ));
        }
    }

    public function actionAktif()
    {
        $id = Yii::app()->user->id;
        $model = Contestant::model()->findByPk($id);
        $model->contestant_last_active = time();
        $model->save();
        echo 'OK';
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}