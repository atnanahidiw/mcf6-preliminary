<?php

class SoalController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('error'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin', 'hapus', 'index', 'lihat', 'tambah', 'tambahSet', 'ubah', 'error'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->layout = 'admin';
        if(!isset($_GET['id'])){
            $model = ProblemSet::model()->findAll();
            $this->render('index',array(
                'model'=>$model
            ));
        }
        else if($_GET['id'] != null){
            $criteria = array(
                'condition' => 'problem_set_id = '.$_GET['id']
            );
            $model = Problem::model()->findAll($criteria);
            $this->render('problem',array(
                'model'=>$model
            ));
        }
    }

    public function actionHapus()
    {
        if(isset($_GET['id']) && ($_GET['id'] != null)){
            $model = Problem::model()->findByPk($_GET['id']);
            $redirect = $model->problem_set_id;
            $model->delete();
            $this->redirect(array('soal/'.$redirect));
        }
    }

    public function actionLihat()
    {
        $this->layout = 'admin';
        if(isset($_GET['id']) && ($_GET['id'] != null)){
            $model = Problem::model()->findByPk($_GET['id']);
            $this->render('view',array(
                'model'=>$model
            ));
        }
    }

    public function actionTambah()
    {
        $model = new Problem;
        if(isset($_POST['Problem'])){
            $model->attributes=$_POST['Problem'];
            $model->save();
            $this->redirect(array('soal/'.$model->problem_set_id));
        }
        if(isset($_GET['id']) && ($_GET['id'] != null)){
            $model->problem_set_id = $_GET['id'];
            $this->renderPartial('problem_form', array(
                'id' => $_GET['id'],
                'model' => $model,
                'action' => Yii::app()->createUrl('/soal/tambah'),
            ));
        }
    }

    public function actionTambahSet()
    {
        $model = new ProblemSet;
        if(isset($_POST['ProblemSet'])){
            $model->attributes=$_POST['ProblemSet'];
            $model->save();
            $this->redirect('index');
        }
        $this->renderPartial('problem_set_form', array(
            'model'  => $model,
        ));
    }

    public function actionUbah()
    {
        if(isset($_GET['id']) && ($_GET['id'] != null)){
            $model = Problem::model()->findByPk($_GET['id']);
            if(isset($_POST['Problem'])){
                $model->attributes=$_POST['Problem'];
                $model->save();
                $this->redirect(array('soal/'.$model->problem_set_id));
            }
            $this->renderPartial('problem_form', array(
                'id' => $_GET['id'],
                'model' => $model,
                'action' => Yii::app()->createUrl('/soal/ubah/'.$_GET['id']),
            ));
        }
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}