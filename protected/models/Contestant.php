<?php

/**
 * This is the model class for table "contestant".
 *
 * The followings are the available columns in table 'contestant':
 * @property integer $contestant_id
 * @property string $contestant_username
 * @property string $contestant_password
 * @property string $contestant_team_name
 * @property integer $problem_set_id
 */
class Contestant extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contestant';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_username', 'required'),
            array('problem_set_id', 'numerical', 'integerOnly'=>true),
            array('contestant_username', 'length', 'max'=>10),
            array('contestant_password', 'length', 'max'=>100),
            array('contestant_team_name', 'length', 'max'=>50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('contestant_id, contestant_username, contestant_password, contestant_team_name, problem_set_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'contestant_id' => 'ID',
            'contestant_username' => 'Username',
            'contestant_password' => 'Password',
            'contestant_team_name' => 'Nama Tim',
            'problem_set_id' => 'Problem Set ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('contestant_username',$this->contestant_username,true);
        $criteria->compare('contestant_password',$this->contestant_password,true);
        $criteria->compare('contestant_team_name',$this->contestant_team_name,true);
        $criteria->compare('problem_set_id',$this->problem_set_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contestant the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
