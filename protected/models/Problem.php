<?php

/**
 * This is the model class for table "problem".
 *
 * The followings are the available columns in table 'problem':
 * @property integer $problem_id
 * @property string $problem_title
 * @property string $problem_description
 * @property string $problem_choice_a
 * @property string $problem_choice_b
 * @property string $problem_choice_c
 * @property string $problem_choice_d
 * @property string $problem_choice_e
 * @property string $problem_choice_right
 * @property integer $problem_set_id
 */
class Problem extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'problem';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('problem_description, problem_choice_a, problem_choice_b, problem_choice_c, problem_choice_d, problem_choice_e, problem_choice_right', 'required'),
            array('problem_set_id', 'numerical', 'integerOnly'=>true),
            array('problem_title', 'length', 'max'=>200),
            array('problem_choice_a, problem_choice_b, problem_choice_c, problem_choice_d, problem_choice_e', 'length', 'max'=>100),
            array('problem_choice_right', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('problem_id, problem_title, problem_description, problem_choice_a, problem_choice_b, problem_choice_c, problem_choice_d, problem_choice_e, problem_choice_right, problem_set_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'problem_id' => 'ID',
            'problem_title' => 'Judul',
            'problem_description' => 'Deskripsi',
            'problem_choice_a' => 'Pilihan A',
            'problem_choice_b' => 'Pilihan B',
            'problem_choice_c' => 'Pilihan C',
            'problem_choice_d' => 'Pilihan D',
            'problem_choice_e' => 'Pilihan E',
            'problem_choice_right' => 'Kunci Jawaban',
            'problem_set_id' => 'Problem Set ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('problem_id',$this->problem_id);
        $criteria->compare('problem_title',$this->problem_title,true);
        $criteria->compare('problem_description',$this->problem_description,true);
        $criteria->compare('problem_choice_a',$this->problem_choice_a,true);
        $criteria->compare('problem_choice_b',$this->problem_choice_b,true);
        $criteria->compare('problem_choice_c',$this->problem_choice_c,true);
        $criteria->compare('problem_choice_d',$this->problem_choice_d,true);
        $criteria->compare('problem_choice_e',$this->problem_choice_e,true);
        $criteria->compare('problem_choice_right',$this->problem_choice_right,true);
        $criteria->compare('problem_set_id',$this->problem_set_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Problem the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
