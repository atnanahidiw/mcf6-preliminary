<?php

/**
 * This is the model class for table "answer".
 *
 * The followings are the available columns in table 'answer':
 * @property integer $answer_id
 * @property integer $contestant_id
 * @property integer $problem_id
 * @property string $answer_choice
 * @property integer $answer_score
 */
class Answer extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'answer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contestant_id, problem_id', 'required'),
            array('contestant_id, problem_id, answer_score', 'numerical', 'integerOnly'=>true),
            array('answer_choice', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('answer_id, contestant_id, problem_id, answer_choice, answer_score', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'answer_id' => 'ID',
            'contestant_id' => 'Contestant ID',
            'problem_id' => 'Problem ID',
            'answer_choice' => 'Choice',
            'answer_score' => 'Score',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('answer_id',$this->answer_id);
        $criteria->compare('contestant_id',$this->contestant_id);
        $criteria->compare('problem_id',$this->problem_id);
        $criteria->compare('answer_choice',$this->answer_choice,true);
        $criteria->compare('answer_score',$this->answer_score);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Answer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
