<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
    * Authenticates a user using the User data model.
    * @return boolean whether authentication succeeds.
    */
    public function authenticate()
    {
        $user=Contestant::model()->findByAttributes(array('contestant_username'=>$this->username));
        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($user->contestant_password!==$this->encrypt($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id = $user->contestant_id;
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function encrypt($value)
    {
//        return $value;
        return md5(md5(md5(md5(md5(md5($value))))));;
    }

    public function getId()
    {
        return $this->_id;
    }
}