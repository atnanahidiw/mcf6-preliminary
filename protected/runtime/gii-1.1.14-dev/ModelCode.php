<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'contestant_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
