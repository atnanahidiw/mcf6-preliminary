<?php
/* @var $this Controller */
$home = Yii::app()->createUrl("/");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Mathematical Challenge Festival (MCF) Institut Teknologi Bandung merupakan acara dwitahunan yang diselenggarakan oleh Himpunan Mahasiswa Matematika (HIMATIKA) ITB sejak tahun 2002. Dalam perkembangannya, MCF ITB telah diadakan sebanyak lima kali.">
        <meta name="keywords" content="MCF, MMC, MCF-MMC, MCF - MMC, MCF6, MCF 6, MCF 6 ITB, MCF ITB, MMC ITB, MCF-MMC ITB, MCF - MMC ITB, MCF 2014, MMC 2014, MCF-MMC 2014, MCF - MMC 2014, MCF ITB 2014, MMC ITB 2014, MCF-MMC ITB 2014, MCF - MMC ITB 2014, Mathematical Challenge Festival, Mathematical Challenge Festival ITB">
        <meta property="og:title" content="MCF-MMC ITB 2014">
        <meta property="og:url" content="http://www.math.itb.ac.id/mcf-mmc/">
        <meta property="og:image" content="http://www.math.itb.ac.id/mcf-mmc/img/logo.jpg">
        <title>Babak Penyisihan MCF-MMC ITB 2014</title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl.'/img/'.(rand(0,1)==1?'mcf':'mmc').'.ico'; ?>">
        <link href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.0.1-p7/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.0.1-p7/js/bootstrap.min.js"></script>
		<script type="text/x-mathjax-config">
		  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
		</script>
		<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    </head>
    <body>

            <section id="layout">
				<div class="admin-menu">
					<ul class="nav nav-pills nav-stacked">
						<li <?php echo (Yii::app()->controller->id=='site') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::app()->createUrl('/site/admin'); ?>"><h4><strong>Halaman Utama</strong></h4></a></li>
						<li <?php echo (Yii::app()->controller->id=='soal') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::app()->createUrl('/soal'); ?>">Soal</a></li>
						<li <?php echo (Yii::app()->controller->id=='peserta') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::app()->createUrl('/peserta'); ?>">Peserta</a></li>
					</ul>
					<a href="<?php echo Yii::app()->createUrl("/site/logout"); ?>">
						<button type="button" id="logout-button" class="btn btn-labeled btn-danger">
							<span class="btn-label"><i class="glyphicon glyphicon-off"></i></span>
							Keluar
						</button>
					</a>
				</div>
                <div class="admin-main"><?php echo $content; ?></div>
            </section>

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.0.1-p7/js/bootstrap.min.js"></script>
    </body>
</html>