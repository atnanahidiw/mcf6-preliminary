<div id="login-panel" class="panel panel-danger">
	<div class="panel-heading"><h4>Tes Login</h4></div>
	<div class="panel-body">
		<form action="<?php echo Yii::app()->createUrl("tes/login"); ?>" method="post" role="form">
			<div class="form-group">
				<label class="sr-only" for="inputUsername">Username</label>
				<input type="text" class="form-control" id="Contestant_contestant_username" name="Contestant[contestant_username]" placeholder="Username">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputPassword">Password</label>
				<input type="password" class="form-control" id="Contestant_contestant_password" name="Contestant[contestant_password]" placeholder="Password">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputName">Nama</label>
				<input type="text" class="form-control" id="Contestant_contestant_team_name" name="Contestant[contestant_team_name]" placeholder="Nama" style="display: none;">
			</div>
			<div style="width: 60px; position:relative; margin: 0 auto;"><input class="btn btn-default btn-sm" type="submit" value="Masuk"></div>
		</form>
	</div>
</div>

<div id="dialog-error" class="modal fade">
  <div class="modal-dialog" style="width: 200px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#Contestant_contestant_password').focus(function(){
		if($('#Contestant_contestant_username').val() == ''){
			$('#Contestant_contestant_username').focus();
		}
		else{
			$.ajax({
				url: "<?php echo Yii::app()->createUrl('/tes/cek') ?>" + "?username=" + $('#Contestant_contestant_username').val(),
				cache: false
			})
			.done(function(result){
				if(result == 'ok'){
					$('#Contestant_contestant_team_name').hide();
					$('.panel-heading').html('<h4>Tes Login</h4>');
					$('#submit-button').val('Masuk');
				}
				else if(result == 'not found'){
					$('#Contestant_contestant_team_name').show();
					$('.panel-heading').html('<h4>Tes Daftar & Login</h4>');
					$('#submit-button').val('Daftar');
				}
			});
		}
	});

	$('#Contestant_contestant_username').focus(function(){
		$(this).removeClass('error-input');
	});
</script>