<div style="width: 800px; margin: 200px auto 0;">
	<div style="width: 600px; margin: 0 auto 20px;"><h3>Tes telah selesai, terima kasih atas partisipasinya ^^</h3></div>
	<div style="width: 700px; margin: 0 auto 50px;">
		<h4>Jika ada kesalahan dalam sistem ini mohon beritahukan kami melalui tautan <a target="_blank" href="http://www.math.itb.ac.id/mcf-mmc/feedback">berikut</a></h4>
	</div>
	<a href="<?php echo Yii::app()->createUrl("/tes/logout"); ?>" style="margin-left:325px;">
		<button type="button" class="btn btn-labeled btn-danger">
			<span class="btn-label"><i class="glyphicon glyphicon-off"></i></span>
			Keluar
		</button>
	</a>
</div>