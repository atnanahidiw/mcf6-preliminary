<div style="width: 500px; margin: 225px auto 0;">
	<div  style="font-size: 20px;">Apakah Anda yakin untuk memasuki sistem penyisihan?</div>
	<div>
		<div>
			<strong>Anda hanya diberikan waktu <?php echo round(Yii::app()->params['total_section']*Yii::app()->params['section_duration']/3600, 2).' jam'; ?> untuk mengerjakan terhitung dari</strong>
		</div>
		<div><strong>Anda menekan tombol Lanjutkan</strong></div>
	</div>
	<div style="margin-top: 50px;">
		<a href="<?php echo Yii::app()->createUrl("/site/logout"); ?>" style="margin-left: 30px;"><button class="btn btn-danger" style="width: 100px;">Batal</button></a>
		<a href="<?php echo Yii::app()->createUrl("/penyisihan/pra"); ?>" style="margin-left: 200px;"><button class="btn btn-success" style="width: 100px;">Lanjutkan</button></a>
	</div>
</div>