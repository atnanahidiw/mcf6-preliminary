<div id="preliminary-nav">
	<div id="preliminary-nav-inner">
		<div id="preliminary-nav-logo">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Untitled-21.png" alt="MCF-MMC 2014" title="Babak Penyisihan">
		</div>
		<div id="preliminary-nav-timer"></div>
		<nav id="preliminary-nav-menu">
			<ul>
				<li id="preliminary-nav-menu-save"><a title="Simpan"><i class="glyphicon glyphicon-floppy-disk"></i></a></li>
				<li id="preliminary-nav-menu-submit"><a title="Kumpulkan"><i class="glyphicon glyphicon-ok"></i></a></li>
				<li><a title="Keluar" href="<?php echo Yii::app()->createUrl("/site/logout"); ?>"><i class="glyphicon glyphicon-off"></i></a></li>
			</ul>
		</nav>
	</div>
</div>
<?php
$i = 0;
$form=$this->beginWidget('CActiveForm', array(
	'id' => 'preliminary-form',
	'htmlOptions' => array(
		'class'   => 'form-horizontal',
		'role'    => 'form',
		'enctype' => 'multipart/form-data',
	)
));
?>

<?php
foreach($problem as $prob) {
	$criteria = array(
		'condition' => 'problem_id = '.$prob->problem_id.' AND contestant_id = '.Yii::app()->user->id,
	);
	$answer = Answer::model()->find($criteria);
	$choice = ($answer == null)?'':$answer->answer_choice;
?>
<div class="preliminary-form-group">
	<div class="preliminary-form-numbering"><?php echo ($start+$i).')'; ?></div>
	<div class="preliminary-form-content">
		<div class="preliminary-form-problem"><?php echo $prob->problem_description?></div>
		<div class="preliminary-form-answer">
			<div class="preliminary-form-choice">
				<input type="radio" name="Answer[<?php echo $i; ?>][answer_choice]" value="A"<?php echo ($choice == 'A')?' checked':'';?>> A. <?php echo $prob->problem_choice_a; ?>
			</div>
			<div class="preliminary-form-choice">
				<input type="radio" name="Answer[<?php echo $i; ?>][answer_choice]" value="B"<?php echo ($choice == 'B')?' checked':'';?>> B. <?php echo $prob->problem_choice_b; ?>
			</div>
			<div class="preliminary-form-choice">
				<input type="radio" name="Answer[<?php echo $i; ?>][answer_choice]" value="C"<?php echo ($choice == 'C')?' checked':'';?>> C. <?php echo $prob->problem_choice_c; ?>
			</div>
			<div class="preliminary-form-choice">
				<input type="radio" name="Answer[<?php echo $i; ?>][answer_choice]" value="D"<?php echo ($choice == 'D')?' checked':'';?>> D. <?php echo $prob->problem_choice_d; ?>
			</div>
			<div class="preliminary-form-choice">
				<input type="radio" name="Answer[<?php echo $i; ?>][answer_choice]" value="E"<?php echo ($choice == 'E')?' checked':'';?>> E. <?php echo $prob->problem_choice_e; ?>
			</div>
		</div>
		<input type="hidden" name="Answer[<?php echo $i; ?>][contestant_id]" value="<?php echo Yii::app()->user->id; ?>">
		<input type="hidden" name="Answer[<?php echo $i; ?>][problem_id]" value="<?php echo $prob->problem_id; ?>">
	</div>
</div>
<?php ++$i; } ?>

<?php $this->endWidget(); ?>

<?php if(isset($_GET['new'])){?>
<div id="pre-dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Jangan lupa</h4>
      </div>
      <div class="modal-body">Anda hanya diberikan waktu mengerjakan maksimal <?php echo round(Yii::app()->params['section_duration']/60,2).' menit'; ?> per sesinya</div>
      <div class="modal-footer">
        <button type="button"class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
	$('#pre-dialog').modal('show');
</script>
<?php } ?>

<script language="javascript" type="text/javascript">
	$('#preliminary-nav-menu-save').click(function(){
		$('form').attr('action', '<?php echo Yii::app()->createUrl('/penyisihan/kumpul'); ?>');
		$('form').submit();
	});

	function submit(){
		$('form').attr('action', '<?php echo Yii::app()->createUrl('/penyisihan/kumpul?new=true'); ?>');
		$('form').submit();
	}
	
	$('#preliminary-nav-menu-submit').click(function(){
		submit();
	});

	//active report
	activeReff = <?php echo Yii::app()->params['active_interval'];?>;
	interval = activeReff;
	
	function active(){
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('/peserta/aktif'); ?>",
			cache: false
		})
		.done(function(result) {
			interval = activeReff;
		});
	}
	
	//timer
	timeLeft = <?php echo $timeLeft; ?>;
	submitTimer = <?php echo $submitTimer; ?>;

	function formatTime(){
		time = timeLeft;
		seconds = (time%60).toString();
		while(seconds.length < 2) seconds = '0' + seconds;
		time = parseInt(time/60);
		minutes = (time%60).toString();
		while(minutes.length < 2) minutes = '0' + minutes;
		time = parseInt(time/60);
		hours = (time%60).toString();
		while(hours.length < 2) hours = '0' + hours;
		$('#preliminary-nav-timer').html(hours+' : '+minutes+' : '+seconds);
	}

	countDown = setInterval(function() {
		//timer
		if(timeLeft > 0){
			--timeLeft;
			formatTime();
		}
		else{
			clearInterval(countDown);
			return;
		}

		//auto submit
		if(submitTimer > 0) --submitTimer;
		if(submitTimer == 0){
			submit();
			clearInterval(countDown);
			return;
		}

		//active report
		if(interval > 0) --interval;
		if(interval == 0) active();
	}, 1000);
</script>