<?php
$form=$this->beginWidget('CActiveForm', array(
	'action'  => Yii::app()->createUrl('/soal/tambahSet'),
	'htmlOptions' => array(
		'class'   => 'form-horizontal',
		'role'    => 'form',
		'enctype' => 'multipart/form-data',
	),
)); 
?>
<?php echo CHtml::errorSummary($model); ?>
<div class="form-group">
	<?php echo $form->label( $model,'problem_set_name', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_set_name', array('class'=>'form-control') ); ?>
	</div>
</div>
<?php $this->endWidget(); ?>