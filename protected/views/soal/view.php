<div class="panel panel-primary">
	<div class="panel-heading admin-main-header"><h4>Soal</h4></div>
	<div class="panel-body admin-main-body">
		<a href="<?php echo Yii::app()->createUrl('/soal/'.$model->problem_set_id); ?>">
		<button type="button" id="back-button" class="btn btn-labeled btn-default">
			<span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
			Kembali
		</button>
		</a>
		<div class="view-main">
			<div class="view-problem">
				<?php echo $model->problem_description; ?>
			</div>
			<div class="view-answer">
				<div class="view-choice<?php echo ($model->problem_choice_right == 'A')?' view-choosen':''; ?>">
					<div class="view-choice-prefix">A. </div>
					<div class="view-choice-content"><?php echo $model->problem_choice_a; ?></div>
				</div>
				<div class="view-choice<?php echo ($model->problem_choice_right == 'B')?' view-choosen':''; ?>">
					<div class="view-choice-prefix">B. </div>
					<div class="view-choice-content"><?php echo $model->problem_choice_b; ?></div>
				</div>
				<div class="view-choice<?php echo ($model->problem_choice_right == 'C')?' view-choosen':''; ?>">
					<div class="view-choice-prefix">C. </div>
					<div class="view-choice-content"><?php echo $model->problem_choice_c; ?></div>
				</div>
				<div class="view-choice<?php echo ($model->problem_choice_right == 'D')?' view-choosen':''; ?>">
					<div class="view-choice-prefix">D. </div>
					<div class="view-choice-content"><?php echo $model->problem_choice_d; ?></div>
				</div>
				<div class="view-choice<?php echo ($model->problem_choice_right == 'E')?' view-choosen':''; ?>">
					<div class="view-choice-prefix">E. </div>
					<div class="view-choice-content"><?php echo $model->problem_choice_e; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
