<div class="panel panel-primary">
	<div class="panel-heading admin-main-header"><h4>Kumpulan Soal</h4></div>
	<div class="panel-body admin-main-body">
		<button type="button" id="toggle-add-button" class="btn btn-labeled btn-primary">
			<span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
			Tambah Kumpulan Soal
		</button>
		<table class="table table-striped table-bordered table-hover table-problem">
			<thead>
				<tr><th>Nama</th><th style="width: 10px;">Banyak Soal</th></tr>
			</thead>
			<tbody data-link="row" class="rowlink">
			<?php
				foreach($model as $mdl){
					$link = Yii::app()->createUrl('/soal').'/'.$mdl->problem_set_id;
					$elmt = '<td><a href="'.$link.'">'.$mdl->problem_set_name.'</a></td>';
					$elmt .= '<td style="text-align: right; padding-right: 20px;">'.Problem::model()->countByAttributes(array('problem_set_id' => $mdl->problem_set_id)).'</td>';
					echo '<tr>'.$elmt.'</tr>';
				}
			?>
			</tbody>
	</table>
	</div>
</div>

<div id="dialog-problem-set" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Tambah Kumpulan Soal</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" id="submit-add-button" class="btn btn-primary">Tambah</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#toggle-add-button').click(function(){
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('/soal/tambahSet'); ?>",
			cache: false
		})
		.done(function(result) {
			$('#dialog-problem-set .modal-body').html(result);
		});
		$('#dialog-problem-set').modal('show');
	});
	
	$('#submit-add-button').click(function(){
		$('form').submit();
	});
</script>