<?php
$form=$this->beginWidget('CActiveForm', array(
	'action'  => $action,
	'htmlOptions' => array(
		'class'   => 'form-horizontal',
		'role'    => 'form',
		'enctype' => 'multipart/form-data',
	),
)); 
?>
<?php echo CHtml::errorSummary($model); ?>
<div class="form-group">
	<?php echo $form->label( $model,'problem_title', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_title', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_description', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textArea($model,'problem_description', array('class'=>'form-control', 'rows'=>'10') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_a', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_a', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_b', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_b', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_c', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_c', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_d', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_d', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_e', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_e', array('class'=>'form-control') ); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'problem_choice_right', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-8">
		<?php echo $form->textField($model,'problem_choice_right', array('class'=>'form-control') ); ?>
	</div>
</div>
<?php echo $form->hiddenField($model,'problem_set_id'); ?>
<?php $this->endWidget(); ?>