<script type="text/javascript">
	function edit(id){
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('/soal/ubah') ?>" + "/" + id,
			cache: false
		})
		.done(function(result) {
			$('#dialog-problem .modal-header .modal-title').html('Ubah Soal')
			$('#dialog-problem .modal-body').html(result);
			$('#dialog-problem .modal-footer button').html('Ubah')
			$('#dialog-problem').modal('show');
		});
	};
</script>

<div class="panel panel-primary">
	<div class="panel-heading admin-main-header"><h4>Soal</h4></div>
	<div class="panel-body admin-main-body">
		<a href="<?php echo Yii::app()->createUrl('/soal'); ?>">
		<button type="button" id="back-button" class="btn btn-labeled btn-default">
			<span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
			Kembali
		</button>
		</a>
		<button type="button" id="toggle-add-button" class="btn btn-labeled btn-primary">
			<span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
			Tambah Soal
		</button>
		<table class="table table-striped table-bordered table-hover table-problem">
			<thead>
				<tr><th>Judul</th><th style="width: 70px;">Menu</th></tr>
			</thead>
			<tbody data-link="row" class="rowlink">
			<?php
				foreach($model as $mdl){
					$link = Yii::app()->createUrl('/soal/lihat/'.$mdl->problem_id);
					$elmt = '<td><a href="'.$link.'">'.$mdl->problem_title.'</a></td>';
				
					$act = '<a style="margin: 0 5px;" title="Ubah" onclick="edit('.$mdl->problem_id.')"><i class="glyphicon glyphicon-wrench"></i></a>';
					$act .= '<a style="margin: 0 5px;" title="Hapus" href="'.Yii::app()->createUrl('/soal/hapus/'.$mdl->problem_id).'"><i class="glyphicon glyphicon-trash"></i></a>';
					
					$elmt .= '<td class="rowlink-skip">'.$act.'</td>';
					echo '<tr>'.$elmt.'</tr>';
				}
			?>
			</tbody>
	</table>
	</div>
</div>

<div id="dialog-problem" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" id="submit-add-button" class="btn btn-primary"></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#toggle-add-button').click(function(){
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('/soal/tambah/'.$_GET['id']); ?>",
			cache: false
		})
		.done(function(result) {
			$('#dialog-problem .modal-header .modal-title').html('Tambah Soal')
			$('#dialog-problem .modal-body').html(result);
			$('#dialog-problem .modal-footer button').html('Tambah')
			$('#dialog-problem').modal('show');
		});
	});

	$('#submit-add-button').click(function(){
		$('form').submit();
	});
</script>