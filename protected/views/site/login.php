<div id="login-panel" class="panel panel-danger">
	<div class="panel-heading"><h4>Login</h4></div>
	<div class="panel-body">
		<form action="<?php echo Yii::app()->createUrl("/"); ?>/" method="post" role="form">
			<div class="form-group">
				<label class="sr-only" for="inputUsername">Username</label>
				<input type="text" class="form-control" id="LoginForm_username" name="LoginForm[username]" placeholder="Username">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputPassword">Password</label>
				<input type="password" class="form-control" id="LoginForm_password" name="LoginForm[password]" placeholder="Password">
			</div>
			<div style="width: 60px; position:relative; margin: 0 auto;"><input class="btn btn-default btn-sm" type="submit" value="Masuk"></div>
		</form>
	</div>
</div>