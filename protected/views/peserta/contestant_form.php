<?php
$form=$this->beginWidget('CActiveForm', array(
	'action'  => Yii::app()->createUrl('/peserta/ubah/'.$model->contestant_id),
	'htmlOptions' => array(
		'class'   => 'form-horizontal',
		'role'    => 'form',
		'enctype' => 'multipart/form-data',
	),
)); 
?>
<?php echo CHtml::errorSummary($model); ?>
<div class="form-group">
	<?php echo $form->hiddenField($model,'contestant_id'); ?>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'contestant_username', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-9 text-input-disabled">
		<?php echo $model->contestant_username; ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->label( $model,'contestant_team_name', array('class'=>'control-label col-md-3') ); ?>
	<div class="col-md-9 text-input-disabled">
		<?php echo 'Tim '.$model->contestant_team_name; ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Kumpulan Soal</label>
	<div class="col-md-8">
		<select class="form-control" name="Contestant[problem_set_id]" id="Contestant_problem_set_id">
			<?php foreach($problemSet as $ps){ ?>
			<option value="<?php echo $ps->problem_set_id; ?>"<?php echo ($ps->problem_set_id==$model->problem_set_id)?' selected':''; ?>><?php echo $ps->problem_set_name; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<?php $this->endWidget(); ?>