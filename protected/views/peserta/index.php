<script type="text/javascript">
	function edit(id){
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('/peserta/ubah'); ?>" + "/" + id,
			cache: false
		})
		.done(function(result) {
			$('#dialog-contestant .modal-body').html(result);
			$('#dialog-contestant').modal('show');
		});
	};
</script>

<div class="panel panel-primary">
	<div class="panel-heading admin-main-header"><h4>Peserta</h4></div>
	<div class="panel-body admin-main-body">
		<a href="<?php echo Yii::app()->createUrl('/peserta/sync'); ?>">
			<button type="button" id="toggle-sync-button" class="btn btn-labeled btn-primary">
				<span class="btn-label"><i class="glyphicon glyphicon-refresh"></i></span>
				Sinkronisasi
			</button>
		</a>
		<?php if(time() > Yii::app()->params['contest_end']) { ?>
		<a href="<?php echo Yii::app()->createUrl('/peserta/nilai'); ?>">
			<button type="button" id="toggle-grade-button" class="btn btn-labeled btn-success">
				<span class="btn-label"><i class="glyphicon glyphicon-pencil"></i></span>
				Nilai Semua
			</button>
		</a>
		<?php } ?>
		<table class="table table-striped table-bordered table-hover table-contestant">
			<thead>
				<tr><th style="width: 40px;">No</th><th>Username</th><th>Nama Tim</th><th style="width: 130px;">Kumpulan Soal</th><th style="width: 50px;">Nilai</th></tr>
			</thead>
			<tbody data-link="row" class="rowlink">
			<?php
				$i = 0;
				foreach($model as $mdl){
					++$i;
					$elmt = '<td style="text-align: right; padding-right: 10px;">'.$i.'</td>';
					$elmt .= '<td><a onclick="edit('.$mdl->contestant_id.')">'.$mdl->contestant_username.'</a></td>';
					$elmt .= '<td>Tim '.$mdl->contestant_team_name.'</td>';

					$problemSet = ProblemSet::model()->findByPk($mdl->problem_set_id);
					$problemSet = ($problemSet != null)?$problemSet->problem_set_name:'';
					$elmt .= '<td>'.$problemSet.'</td>';

					$score = Yii::app()->db->createCommand('SELECT SUM(answer_score) as score FROM `answer` WHERE contestant_id = '. $mdl->contestant_id)->queryAll();
					$elmt .= '<td style="text-align: right; padding-right: 20px;">'.$score[0]['score'].'</td>';
					echo '<tr>'.$elmt.'</tr>';
				}
			?>
			</tbody>
	</table>
	</div>
</div>

<div id="dialog-contestant" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Ubah Peserta</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" id="submit-edit-button" class="btn btn-primary">Ubah</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#submit-edit-button').click(function(){
		$('form').submit();
	});
</script>